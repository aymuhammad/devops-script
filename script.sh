#!/bin/bash
echo "welcome to the 1st Devops Class"
echo "Need administrative Privileges. (y/n)"
read accessDecision
if [ $accessDecision == y ]
then
	if [ "$UID" -ne 0 ]
	then exec sudo -S "$0" "$0"
fi
echo "What is your first name"
read firstname
adduser $firstname
usermod -aG sudo $firstname
apt update
ufw app list
ufw allow OpenSSH
ufw enable
ufw status
touch -p ~/.ssh/authorized_keys
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYNYf60Tt+t2P8xbwX9j2ODh6f+wupgAbH3uTYDIBgsiVLsgQFjdWaLvBWv2JdqQfR+OfsMERvbbTT3uHBR2FdEvXZ15s6DPERhTmBprRzKMReGj6OpgO/c5iy6CGkAncNmRQ6tL9WIu7G6L7gWldvMavrhveJ8JXcvjN++c+e9C82TkQ0mYsrRX29FRfqeqyQ/yTheYiTuudy8iMwxUVWgObRBm2CDPTVfFSLP9eC5pI9WXQlG7/hReocL4BzEjHNBcGDVtLs9GQauFRLQBHKlX+dTilRBSwWP2JS9C74feq5Mk32pkP4cJIFAsHuQ6otVwftMwdTzET1GbiGu6C9" >> ~/.ssh/authorized_keys 
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDCdRqF2VWwRYB464zy/OEruHxeApFAGRjuSm1f1WHNRBY69OuTTJVg0+Koj8mOp55Nn9xhSvi5ES2cpTFFfQUtGsPJv8oKNm0/9bvlar7re0XM0/EuoeExpTPNy4r2Dfq/WPt09QSTkDtnwXCZ3jjqeEt+y54Qf8C7BDf426C/wwqAzW7zu61DU2fojX8v1xElgkP4du2o32sCQtsOc+ZCdawmqRM4sOF8EpQZSg8q+xfAoSZv102GD4/hxQlkFEAeKddrPE2aQynXyR42pSD9mzvZZALujvRdXh40DKBoRR03wtgIKHBp9Sq05oG5Bh/HLtJa2amt034ODQCtL33P" >> ~/.ssh/authorized_keys
chmod -R go= ~/.ssh
sudo ufw allow ssh
sudo ufw enable -y
sudo apt update
sudo apt install apache2 -y
sudo ufw app list
sudo ufw app info "Apache Full"
sudo ufw allow in "Apache Full"
sudo apt install mysql-server -y
sudo mysql_secure_installation -y
sudo apt install php libapache2-mod-php php-mysql -y
sudo nano /etc/apache2/mods-enabled/dir.conf
echo "<IfModule mod_dir.c>
		DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
      </IfModule>"
sudo systemctl restart apache2
sudo systemctl status apache2
sudo apt install php-cli -y
touch /var/www/html/info.php
echo "<?php
phpinfo();
?>" >> /var/www/html/info.php
sudo rm /var/www/html/info.php
mkdir sysinfo
git clone https://github.com/phpsysinfo/phpsysinfo.git && cp /sysinfo/phpsysinfo/phpsysinfo.ini.new
cp sysinfo/phpsysinfo /var/www/html/